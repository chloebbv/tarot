/**
 * Génère un nombre aléatoire compris entre un minimum et un maximum et le retourne
 * 
 * @param {number} min 
 * @param {number} max 
 */
function getRandomInteger(min, max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Parcourt un ensemble de données pour retourner la ligne correspondant à l'index spécifié
 * 
 * @param {number} index : index qu'on souhaite atteindre dans le set
 * @param {Set} set : ensemble de données duquel on souhaite extraire une ligne de donnée
 */
function atIndex(index, set){
    //fonction qui renvoie la valeur du set passé en paramètre à l'indice passé en paramètre
    let newValue;
    let values = set.values()
    for (let i = 0; i <= index; i++){
        newValue = values.next().value
    }
    console.log(newValue);
    return newValue;   
}

/**
 * Crée une <li> qui affiche l'image et les infos de la carte tirée dans la ul#tirage
 * 
 * @param {number} nb : nombre généré aléatoirement
 * @param {object} card : une ligne de l'ensemble de données (set) arcanes
 */
function displayCard(nb, card){
    //On crée une li pour la valeur retournée par atIndex qui contient l'image correspondant à l'index réel, le numéro, le nom et la signification de la carte, cette li devient un enfant de l'ul.
    let li = document.createElement('li');
    li.innerHTML = `<img src="images/${nb}.jpg" alt="${card.nom}">
    <h2>${card.num} - ${card.nom}</h2>
    <p>${card.signification}</p>`
    document.querySelector('#tirage').appendChild(li);
}

/**
 * Fonction appelée quand on clique sur le bouton "Tire moi une carte"
 * Se charge de tirer une carte aléatoire et d'appeler la fonction qui l'affiche dans la ul
 * Au bout de 3 cartes tirées, le bouton disparait et un autre permettant de relancer la page apparait
 */
function pickCard(){     
    let nb;
    do {
        /**
         * tire un nombre aléatoire compris entre 1 et la taille du set contenant la liste des arcanes
         * si cette carte a déjà été tiré, le système en tire une autre
         */
        nb = getRandomInteger(1, arcanes.size);
    } while (tirage.has(nb))

    /**
     * ajoute le numéro de la carte dans le set contenant tous les numéros de cartes tirées
     */
    tirage.add(nb);
    console.log(nb)
    //On diminue nb de 1 pour avoir un nombre compris entre 0 et 21 -> correspond à l'index réel
    nb = nb - 1;
    /**
     * les Set n'ont pas de méthodes permettant de récupérer la valeur se trouvant à un index en particulier, comme les tableaux. 
     * Nous appelons donc une fonction (atIndex) qui permet de reproduire cette fonctionnalité en lui passant l'index souhaité et l'ensemble de données à parcourir. 
     * Cette fonction a pour objectif de nous retourner l'item se trouvant à la ligne choisie
     */
    //Card a pour valeur la valeur retournée par la fonction atIndex
    let card = atIndex(nb, arcanes);
    
    /**
     * Appelle la fonction qui permet d'afficher la carte tirée
     */
    //Appel de la fonction display card qui prend en paramètres l'index réel et la valeur retournée par atIndex
    displayCard(nb, card)

    /**
     * Si 3 cartes ont été tirées :
     * on cache le bouton #go pour ne pas pouvoir en tirer d'autre
     * on affiche le bouton #replay qui permet de lancer un autre tirage
     */
    if(tirage.size == 3){
        document.querySelector("#go").classList.add("hide");
        document.querySelector("#replay").classList.remove("hide");
    } 
}


let tirage = new Set(); //variable qui contiendra les numéros des cartes tirées

/**
 * Code qui ne s'éxécute que quand la page est chargée
 */
document.addEventListener('DOMContentLoaded', function(){
    //gestionnaire d'événement sur le bouton #go qui permet de tirer une carte et l'afficher (appel de la fonction pickCard)
    document.querySelector('#go').addEventListener('click', pickCard);

    //gestionnaire d'événement sur le bouton #replay qui permet de rafraichir la page
    document.querySelector("#replay").addEventListener('click', function(){
        window.location.reload();
    });
});